import sys
from collections import OrderedDict

from cron_converter import Cron
from cron_validator import CronValidator

sys.path.append('../')

from config.main_config import logging, standard_cron, column_width, month_map


class CronParser:
    """
    Class to parse cron string to and expands each field to show the times at which given command will run

    Attributes:
        :argument: str
            input cron string
    Methods:
        pre_process_argument:
            pre-process cron string
        is_cron_exp_valid:
            checks if provided cron exp is valid
        get_cron_instance:
            generated cron instance from cron exp
        get_cron_exp_parsed:
            parse cron instance
        parse_cron_to_table:
            format the cron instance in tabular form
        run_cron_parser:
            run cron parser as a pipeline
    """

    def __init__(self, argument):
        self.argument = argument or ''
        self.cron_exp = None
        self.command = None
        self.cron_instance = None

    def pre_process_argument(self):
        """
        Pre-process cron input argument and get command and cron expression
        :return: None
        """
        if self.argument:
            exp_command = self.argument.split(' ')
            self.command = exp_command[-1]
            cron_exp = exp_command[:-1]  # excluding the command
            self.cron_exp = ' '.join(cron_exp)
        else:
            sys.exit(1)

    def is_cron_exp_valid(self):
        """
        Checks if input cron expression is valid
        :return: True for valid expression
        """

        try:
            assert CronValidator.parse(self.cron_exp) is not None
            return True
        except ValueError:
            logging.error('cron expression is not valid, please validate and retry with valid expression, exiting!')
            sys.exit(1)

    def get_cron_instance(self):
        """
        Generated cron instance of scheduled in list format
        Example:
            cron_exp = '*/10 9-17 1 * *'
            cron_instance =  [
                              [ 0, 10, 20, 30, 40, 50 ],
                              [ 9, 10, 11, 12, 13, 14, 15, 16, 17 ],
                              [ 1 ],
                              [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ],
                              [ 0, 1, 2, 3, 4, 5, 6 ]
                            ]
        """
        self.cron_instance = Cron(self.cron_exp).to_list()

    def get_cron_exp_parsed(self) -> OrderedDict:
        """
        Created parsed cron expression map as per standard cron format
        :return: cron_parsed: dict

        Example:
            cron_parsed = OrderedDict(
                                        [
                                            ("minute", [0, 15, 30, 45]),
                                            ("hour", [0]),
                                            ("day of month", [1, 15]),
                                            ("month", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
                                            ("day of week", [1, 2, 3, 4, 5]),
                                            ("command", "/usr/bin/find"),
                                        ]
                                    )
        """
        cron_parsed = OrderedDict()
        for cron_schedule, cron_exp in zip(standard_cron, self.cron_instance):
            cron_parsed[cron_schedule] = cron_exp
        cron_parsed['command'] = self.command
        return cron_parsed

    def parse_cron_to_table(self, cron_parsed: dict):
        """
        Parse cron map expression to tabular form
        :param cron_parsed: OrderedDict: parsed map of cron expression

        Example:
                minute         0 15 30 45
                hour           0
                day of month   1 15
                month          1 2 3 4 5 6 7 8 9 10 11 12
                day of week    1 2 3 4 5
                command        /usr/bin/find

        """
        print('\n')
        for key, schedule in cron_parsed.items():
            # append the string with the number of spaces required
            if not key == 'command':
                pad = ' '.join(str(interval) for interval in schedule)
            else:
                pad = self.command
            row_padded = key + " " * (column_width - len(key)) + " " + pad
            print(row_padded)

    def run_cron_parser(self):
        """
        Runs the cron string parser as a pipeline
        """
        self.pre_process_argument()
        if self.is_cron_exp_valid():
            self.get_cron_instance()
            cron_parsed = self.get_cron_exp_parsed()
            self.parse_cron_to_table(cron_parsed)
            return True


if __name__ == "__main__":
    input_argument = sys.argv[1]
    if input_argument:
        cron_parser_instance = CronParser(input_argument)
        cron_parser_instance.run_cron_parser()
    else:
        logging.error('please enter cron expression and retry!')
        sys.exit(1)
