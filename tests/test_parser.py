import sys
from contextlib import contextmanager
from io import StringIO

import pytest

sys.path.append('../')
from src.parser import CronParser


@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


class TestCronParser:

    def test_empty_argument(self):
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            cron_obj = CronParser('')
            cron_obj.run_cron_parser()
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def test_invalid_cron_argument(self):
        invalid_cron_argument = '*/61 * * * * /usr/find/bin'
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            cron_obj = CronParser(invalid_cron_argument)
            cron_obj.run_cron_parser()
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def test_valid_cron_argument(self):
        valid_cron_argument = '*/10 9-17 1 * * /usr/find/bin'
        valid_output = """minute         0 10 20 30 40 50\nhour           9 10 11 12 13 14 15 16 17\nday of month   1\nmonth          1 2 3 4 5 6 7 8 9 10 11 12\nday of week    0 1 2 3 4 5 6\ncommand        /usr/find/bin"""
        cron_obj = CronParser(valid_cron_argument)
        with captured_output() as (out, err):
            cron_obj.run_cron_parser()
        output_test = out.getvalue().strip()
        assert output_test == valid_output

