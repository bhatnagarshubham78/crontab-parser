import logging
import os
import sys

# static logging attributes
log_format = '%(asctime)s | %(levelname)s | %(filename)s | %(message)s'
log_level = logging.INFO

# Logging level
logging.basicConfig(stream=sys.stdout, level=log_level, format=log_format)

home_dir = os.path.abspath(os.path.join(__file__, "../../"))
config_dir = home_dir + "/config/"

column_width = 14
standard_cron = ['minute', 'hour', 'day of month', 'month', 'day of week']

max_range_map = {
    'minute': 59,
    'hour': 23
}

min_range_map = {
    'minute': 0,
    'hour': 0
}


month_map = {
    'jan': 1,
    'feb': 2,
    'mar': 3,
    'apr': 4,
    'may': 5,
    'jun': 6,
    'jul': 7,
    'aug': 8,
    'sep': 9,
    'oct': 10,
    'nov': 11,
    'dec': 12
}
